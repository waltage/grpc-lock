from google.protobuf import duration_pb2 as _duration_pb2
from google.protobuf import empty_pb2 as _empty_pb2
from grpc_lock.protos import gcs_lock_pb2 as _gcs_lock_pb2
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class NewLockRequest(_message.Message):
    __slots__ = ("bucket", "object", "duration")
    BUCKET_FIELD_NUMBER: _ClassVar[int]
    OBJECT_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    bucket: str
    object: str
    duration: _duration_pb2.Duration
    def __init__(self, bucket: _Optional[str] = ..., object: _Optional[str] = ..., duration: _Optional[_Union[_duration_pb2.Duration, _Mapping]] = ...) -> None: ...

class ExtendLockRequest(_message.Message):
    __slots__ = ("lock", "duration")
    LOCK_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    lock: _gcs_lock_pb2.GCSLock
    duration: _duration_pb2.Duration
    def __init__(self, lock: _Optional[_Union[_gcs_lock_pb2.GCSLock, _Mapping]] = ..., duration: _Optional[_Union[_duration_pb2.Duration, _Mapping]] = ...) -> None: ...
