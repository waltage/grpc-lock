import datetime as dt

from google.cloud.exceptions import Forbidden
from google.cloud.exceptions import NotFound
from google.cloud.exceptions import Unauthorized
from google.cloud.storage import Blob
from google.protobuf import timestamp_pb2

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.server.constants import LOCK_EXPIRES_TAG_NAME
from grpc_lock.server.constants import LOCK_ID_TAG_NAME
from grpc_lock.server.constants import LOCK_MAGIC_CRC32C
from grpc_lock.server.exceptions import LockContentsMismatch
from grpc_lock.server.exceptions import LockExpired
from grpc_lock.server.exceptions import LockGenerationMismatch
from grpc_lock.server.exceptions import LockIDMismatch
from grpc_lock.server.exceptions import LockMissingTag
from grpc_lock.server.exceptions import LockObjectNotFound
from grpc_lock.server.exceptions import MissingGCSPermissions


def _cmp_timestamp_tags(later: str, earlier: str) -> bool:
    later_parts = later.split(".")
    earlier_parts = earlier.split(".")

    if int(later_parts[0]) < int(earlier_parts[0]):
        return False
    elif int(later_parts[0]) > int(earlier_parts[0]):
        return True

    # if seconds are equal...
    if int(later_parts[1]) <= int(earlier_parts[1]):
        return False
    return True


def make_timestamp(datetime_obj: dt.datetime) -> timestamp_pb2.Timestamp:
    """Converts a datetime object to a protobuf timestamp."""
    timestamp = timestamp_pb2.Timestamp()
    timestamp.FromDatetime(datetime_obj)
    return timestamp


def now_as_expires_tag() -> str:
    """Generates a string representing the UTC now protobuf Timestamp."""
    now_dt = dt.datetime.now(dt.timezone.utc)
    now_pb = make_timestamp(now_dt)
    return f"{now_pb.seconds}.{now_pb.nanos}"


def blob_is_lock(blob: Blob) -> None:
    """Validates whether the blob is a GRPC lock.  Raises if not.

    Criteria:
        - Object must exist
        - Checksum must match the magic we expect
        - ID tag must be present
        - Expires tag must be present

    Raises:
        - LockObjectNotFound
        - MissingGCSPermissions
        - LockMissingIDTag
        - LockMissingExpiresTag
        - LockContentsMismatch
    """

    try:
        blob.reload()
    except NotFound as e:
        raise LockObjectNotFound() from e
    except (Forbidden, Unauthorized) as e:
        raise MissingGCSPermissions() from e

    if LOCK_ID_TAG_NAME not in blob.metadata:
        raise LockMissingTag(LOCK_ID_TAG_NAME)

    if LOCK_EXPIRES_TAG_NAME not in blob.metadata:
        raise LockMissingTag(LOCK_EXPIRES_TAG_NAME)

    if blob.crc32c != f"{LOCK_MAGIC_CRC32C}":
        raise LockContentsMismatch(blob.crc32c)


def blob_is_valid(blob: Blob, lock: GCSLock) -> None:
    """Validates the lock gcs object against the expected values of the lock protobuf."""
    if blob.generation != lock.generation:
        raise LockGenerationMismatch(lock.generation, blob.generation)

    if blob.metadata[LOCK_ID_TAG_NAME] != lock.lock_id:
        raise LockIDMismatch(lock.lock_id, blob.metadata[LOCK_ID_TAG_NAME])

    expires_str = blob.metadata[LOCK_EXPIRES_TAG_NAME]
    if _cmp_timestamp_tags(
        later=now_as_expires_tag(),
        earlier=expires_str,
    ):
        delete_lock_if_expired(blob)
        raise LockExpired(expires_str)

    # Update the lock's expiration time (incase it has changed)
    parts = expires_str.split(".")
    expires_pb = timestamp_pb2.Timestamp(seconds=int(parts[0]), nanos=int(parts[1]))
    lock.expires.CopyFrom(expires_pb)


def delete_lock_if_expired(blob: Blob) -> None:
    """Deletes the blob if it has expired.  Verifies this is a lock first."""
    try:
        blob_is_lock(blob)
    except:
        return

    now_dt = dt.datetime.now(dt.timezone.utc)
    now_pb = make_timestamp(now_dt)
    now_str = f"{now_pb.seconds}.{now_pb.nanos}"

    if _cmp_timestamp_tags(
        later=blob.metadata[LOCK_EXPIRES_TAG_NAME],
        earlier=now_str,
    ):
        # Not expired
        return

    blob.delete()
