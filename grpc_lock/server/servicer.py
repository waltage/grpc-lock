# fmt: off

from typing import TYPE_CHECKING

from google.cloud.storage import Client as GCSClient
from google.protobuf.empty_pb2 import Empty
from grpc import ServicerContext
from grpc import StatusCode

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.protos.grpc_lock_service_pb2 import ExtendLockRequest
from grpc_lock.protos.grpc_lock_service_pb2 import NewLockRequest
from grpc_lock.protos.grpc_lock_service_pb2_grpc import GrpcLockServiceServicer
from grpc_lock.protos.grpc_lock_service_pb2_grpc import add_GrpcLockServiceServicer_to_server
from grpc_lock.server.exceptions import AbortRpcFromException
from grpc_lock.server.handlers.certify_lock import certify_lock_handler
from grpc_lock.server.handlers.extend_lock import extend_lock_handler
from grpc_lock.server.handlers.new_lock import new_lock_handler
from grpc_lock.server.handlers.release_lock import release_lock_handler

# fmt: on

if TYPE_CHECKING:
    _ = add_GrpcLockServiceServicer_to_server


class GrpcLockServicerImplementation(GrpcLockServiceServicer):

    def __init__(self, gcs_client: GCSClient):
        self.gcs_client = gcs_client

    def NewLock(self, request: NewLockRequest, context: ServicerContext) -> GCSLock:
        """Mint a new lock object."""
        try:
            result = new_lock_handler(request, self.gcs_client)
            context.set_code(StatusCode.OK)
            return result

        except Exception as e:
            AbortRpcFromException(context, e)

    def CertifyLock(self, request: GCSLock, context: ServicerContext) -> GCSLock:
        """Verify that a lock is valid."""
        try:
            result = certify_lock_handler(request, self.gcs_client)
            context.set_code(StatusCode.OK)
            return result

        except Exception as e:
            AbortRpcFromException(context, e)

    def ReleaseLock(self, request: GCSLock, context: ServicerContext) -> Empty:
        try:
            result = release_lock_handler(request, self.gcs_client)
            context.set_code(StatusCode.OK)
            return result

        except Exception as e:
            AbortRpcFromException(context, e)

    def ExtendLock(
        self, request: ExtendLockRequest, context: ServicerContext
    ) -> GCSLock:
        try:
            result = extend_lock_handler(request, self.gcs_client)
            context.set_code(StatusCode.OK)
            return result

        except Exception as e:
            AbortRpcFromException(context, e)
