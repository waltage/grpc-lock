"""Exceptions for the gRPC Lock service."""

import json
from typing import Optional

from grpc import ServicerContext
from grpc import StatusCode


class GrpcLockException(Exception):
    """GrpcLockException is the base class for all exceptions in this module."""

    def __init__(self, code: StatusCode, message: str):
        self.code = code
        self.message = message
        self.exception = self.__cause__ if self.__cause__ else "___none___"
        super().__init__(f"{self.code.name}: {message}")


class LockAlreadyHeld(GrpcLockException):
    """A different service already holds the lock."""

    def __init__(self, bucket: str, object: str):
        super().__init__(
            StatusCode.ALREADY_EXISTS,
            f"lock already held on gs://{bucket}/{object}",
        )


class LockBucketNotFound(GrpcLockException):
    """GCS Bucket was not found."""

    def __init__(self, bucket: str):
        super().__init__(
            StatusCode.NOT_FOUND,
            f"bucket gs://{bucket} not found",
        )


class MissingGCSPermissions(GrpcLockException):
    """The service account lacks the necessary permissions."""

    def __init__(self):
        super().__init__(
            StatusCode.PERMISSION_DENIED,
            "missing GCS permissions",
        )


class LockObjectNotFound(GrpcLockException):
    """GCS Object was not found."""

    def __init__(self):
        super().__init__(
            StatusCode.NOT_FOUND,
            "gcs object not found",
        )


class LockGenerationMismatch(GrpcLockException):
    """GCS Object generation mismatch."""

    def __init__(self, expected_generation: int, actual_generation: int):
        super().__init__(
            StatusCode.FAILED_PRECONDITION,
            (
                f"gcs object generation difference; is this lock stale? "
                f"got({actual_generation}), expected({expected_generation})"
            ),
        )


class LockMissingTag(GrpcLockException):
    """GCS Object is missing an expected/necessary tag."""

    def __init__(self, tag_name: str):
        super().__init__(
            StatusCode.FAILED_PRECONDITION,
            f"gcs object is missing a tag ({tag_name})",
        )


class LockIDMismatch(GrpcLockException):
    """GCS Object ID mismatch."""

    def __init__(self, expected_id: str, actual_id: str):
        super().__init__(
            StatusCode.FAILED_PRECONDITION,
            (
                f"gcs object has a different ID; is this lock stale? "
                f"got({actual_id}), expected({expected_id})"
            ),
        )


class LockContentsMismatch(GrpcLockException):
    """GCS Object is likely not a GCSLock."""

    def __init__(self, actual_crc32c: str):
        super().__init__(
            StatusCode.FAILED_PRECONDITION,
            (
                f"gcs object contents unrecognized; is this a gcs lock? "
                f"got({actual_crc32c})"
            ),
        )


class LockExpired(GrpcLockException):
    """GCS Object has an expiration time tag that is expired."""

    def __init__(self, expired_str: str):
        super().__init__(
            StatusCode.DEADLINE_EXCEEDED,
            (f"this lock has expired at {expired_str};" " it has been removed."),
        )


def AbortRpcFromException(
    context: ServicerContext,
    exception: Optional[Exception] = None,
):
    """Automatically aborts the RPC with the appropriate error code and message."""
    if isinstance(exception, GrpcLockException):
        context.abort(
            exception.code,
            json.dumps(
                {
                    "message": exception.message,
                    "exception_type": exception.__class__.__name__,
                    "exception_str": str(exception.exception),
                }
            ),
        )
    else:
        context.abort(
            StatusCode.UNKNOWN,
            json.dumps(
                {
                    "message": "unknown error",
                    "exception_type": (
                        exception.__class__.__name__ if exception else "___none___"
                    ),
                    "exception_str": str(exception) if exception else "___none___",
                }
            ),
        )
