from google.cloud.storage import Client as GCSClient

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.server.util import blob_is_lock
from grpc_lock.server.util import blob_is_valid


def certify_lock_handler(
    lock: GCSLock,
    gcs_client: GCSClient,
) -> GCSLock:

    blob = gcs_client.bucket(lock.bucket).blob(lock.object)

    blob_is_lock(blob)
    blob_is_valid(blob, lock)

    return lock
