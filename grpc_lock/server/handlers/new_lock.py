import datetime as dt
from uuid import uuid4

from google.cloud.exceptions import Forbidden
from google.cloud.exceptions import NotFound
from google.cloud.exceptions import PreconditionFailed
from google.cloud.exceptions import Unauthorized
from google.cloud.storage import Client as GCSClient

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.protos.grpc_lock_service_pb2 import NewLockRequest
from grpc_lock.server.constants import LOCK_EXPIRES_TAG_NAME
from grpc_lock.server.constants import LOCK_ID_TAG_NAME
from grpc_lock.server.constants import LOCK_MAGIC
from grpc_lock.server.exceptions import LockAlreadyHeld
from grpc_lock.server.exceptions import LockBucketNotFound
from grpc_lock.server.exceptions import MissingGCSPermissions
from grpc_lock.server.util import delete_lock_if_expired
from grpc_lock.server.util import make_timestamp


def new_lock_handler(
    request: NewLockRequest,
    gcs_client: GCSClient,
) -> GCSLock:

    result = GCSLock(
        bucket=request.bucket,
        object=request.object,
        lock_id=str(uuid4()),
        expires=make_timestamp(
            dt.datetime.now(dt.timezone.utc)
            + dt.timedelta(
                seconds=request.duration.seconds + 1e-9 * request.duration.nanos,
            )
        ),
    )

    blob = gcs_client.bucket(request.bucket).blob(request.object)

    try:
        delete_lock_if_expired(blob)
        # this next line here is the linchpin of the whole thing.
        # it warrants a comment.  we write an object iff an object
        # does not already exist (generation = 0 precondition check).
        # this is, more or less, exposing chubby.
        blob.upload_from_string(LOCK_MAGIC, if_generation_match=0)
        blob.reload()
        blob.metadata = {
            LOCK_ID_TAG_NAME: result.lock_id,
            LOCK_EXPIRES_TAG_NAME: f"{result.expires.seconds}.{result.expires.nanos}",
        }
        blob.patch()
        result.generation = blob.generation
        return result

    except PreconditionFailed as e:
        raise LockAlreadyHeld(request.bucket, request.object) from e
    except NotFound as e:
        raise LockBucketNotFound(request.bucket) from e
    except (Unauthorized, Forbidden) as e:
        raise MissingGCSPermissions() from e
    except Exception as e:
        raise e
