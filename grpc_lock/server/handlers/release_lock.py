from google.cloud.storage import Client as GCSClient
from google.protobuf.empty_pb2 import Empty

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.server.util import blob_is_lock
from grpc_lock.server.util import blob_is_valid


def release_lock_handler(
    lock: GCSLock,
    gcs_client: GCSClient,
) -> Empty:
    blob = gcs_client.bucket(lock.bucket).blob(lock.object)

    blob_is_lock(blob)
    blob_is_valid(blob, lock)

    # All good... delete it
    blob.delete()

    return Empty()
