import datetime as dt

from google.cloud.storage import Client as GCSClient

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.protos.grpc_lock_service_pb2 import ExtendLockRequest
from grpc_lock.server.constants import LOCK_EXPIRES_TAG_NAME
from grpc_lock.server.util import blob_is_lock
from grpc_lock.server.util import blob_is_valid
from grpc_lock.server.util import make_timestamp


def extend_lock_handler(request: ExtendLockRequest, gcs_client: GCSClient) -> GCSLock:
    blob = gcs_client.bucket(request.lock.bucket).blob(request.lock.object)

    blob_is_lock(blob)
    blob_is_valid(blob)

    now_dt = dt.datetime.now(dt.timezone.utc)
    new_expires_dt = now_dt + dt.timedelta(
        seconds=request.duration.seconds + 1e-9 * request.duration.nanos
    )
    new_expires_pb = make_timestamp(new_expires_dt)
    blob.metadata[LOCK_EXPIRES_TAG_NAME] = (
        f"{new_expires_pb.seconds}.{new_expires_pb.nanos}"
    )
    blob.patch()
    request.lock.expires.CopyFrom(new_expires_pb)
    return request.lock
