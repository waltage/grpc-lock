import datetime as dt
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from google.cloud.exceptions import Forbidden
from google.cloud.exceptions import NotFound
from google.cloud.exceptions import Unauthorized
from google.protobuf.timestamp_pb2 import Timestamp

from grpc_lock.protos.gcs_lock_pb2 import GCSLock
from grpc_lock.server.constants import LOCK_EXPIRES_TAG_NAME
from grpc_lock.server.constants import LOCK_ID_TAG_NAME
from grpc_lock.server.constants import LOCK_MAGIC_CRC32C

from .exceptions import LockContentsMismatch
from .exceptions import LockExpired
from .exceptions import LockGenerationMismatch
from .exceptions import LockIDMismatch
from .exceptions import LockMissingTag
from .exceptions import LockObjectNotFound
from .exceptions import MissingGCSPermissions
from .util import _cmp_timestamp_tags
from .util import blob_is_lock
from .util import blob_is_valid
from .util import delete_lock_if_expired
from .util import make_timestamp
from .util import now_as_expires_tag


def test_make_timestamp_returns_correct_pb():
    datetime_obj = dt.datetime(1970, 1, 1, 0, 0, 0, 0, dt.timezone.utc)

    result = make_timestamp(datetime_obj)

    assert isinstance(result, Timestamp)
    assert result.seconds == 0
    assert result.nanos == 0

    datetime_obj_nanos = datetime_obj + dt.timedelta(seconds=1e-6)

    result_nanos = make_timestamp(datetime_obj_nanos)

    assert isinstance(result_nanos, Timestamp)
    assert result_nanos.seconds == 0
    assert result_nanos.nanos == 1000


def test_now_as_expires_tag_returns_correct_str():

    with patch("grpc_lock.server.util.dt") as dt_mock:
        dt_mock.datetime.now.return_value = dt.datetime(
            1970, 1, 1, 0, 0, 0, 0, dt.timezone.utc
        )

        result = now_as_expires_tag()

        assert isinstance(result, str)
        assert result == "0.0"

        dt_mock.datetime.now.return_value = dt.datetime(
            1970, 1, 1, 0, 0, 0, 0, dt.timezone.utc
        ) + dt.timedelta(seconds=1e-6 + 1)

        result_nanos = now_as_expires_tag()

        assert isinstance(result_nanos, str)
        assert result_nanos == "1.1000"


def test_blob_is_lock_passes_with_valid_blob():
    """Ensure canonical lock objects pass.

    Must have:
        - LOCK_ID_TAG
        - LOCK_EXPIRES_TAG
        - A CRC32C that matches
    """
    blob = Mock()
    blob.metadata = {
        LOCK_ID_TAG_NAME: "123",
        LOCK_EXPIRES_TAG_NAME: "1000.0",
    }
    blob.crc32c = LOCK_MAGIC_CRC32C

    blob_is_lock(blob)

    blob.reload.assert_called()


def test_blob_is_lock_raises_correct_exceptions_on_gcs_exceptions():
    blob = Mock()
    blob.reload.side_effect = NotFound("missing")

    with pytest.raises(LockObjectNotFound) as e:
        blob_is_lock(blob)

        assert isinstance(e.value.__cause__, NotFound)

    blob.reload.side_effect = Forbidden("nuh, uh")

    with pytest.raises(MissingGCSPermissions) as e:
        blob_is_lock(blob)

        assert isinstance(e.value.__cause__, Forbidden)

    blob.reload.side_effect = Unauthorized("get out of here")

    with pytest.raises(MissingGCSPermissions) as e:
        blob_is_lock(blob)

        assert isinstance(e.value.__cause__, Unauthorized)


@pytest.fixture
def canonical_gcs_object():
    def new_obj():
        blob = Mock()
        blob.metadata = {
            LOCK_EXPIRES_TAG_NAME: "1000.0",
            LOCK_ID_TAG_NAME: "a_lock yo",
        }
        blob.crc32c = LOCK_MAGIC_CRC32C
        return blob

    return new_obj


def test_blob_is_lock_raises_correct_exceptions_on_bad_object_properties(
    canonical_gcs_object,
):
    obj_no_id = canonical_gcs_object()
    del obj_no_id.metadata[LOCK_ID_TAG_NAME]

    with pytest.raises(LockMissingTag) as e:
        blob_is_lock(obj_no_id)

    obj_no_expires = canonical_gcs_object()
    del obj_no_expires.metadata[LOCK_EXPIRES_TAG_NAME]

    with pytest.raises(LockMissingTag) as e:
        blob_is_lock(obj_no_expires)

    obj_bad_crc32c = canonical_gcs_object()
    obj_bad_crc32c.crc32c = "not the magic"

    with pytest.raises(LockContentsMismatch) as e:
        blob_is_lock(obj_bad_crc32c)


@pytest.mark.parametrize(
    "now,expires,expected",
    [
        ("1.0", "0.0", True),
        ("01.11", "1.0", True),
        ("100000000.0", "17.234", True),
        ("0.0", "0.0", False),
        ("0.0", "1.0", False),
        ("-500.0", "-700.0", True),
    ],
)
def test_expires_comparator_func_returns_correct_values(
    now: str, expires: str, expected
):
    result = _cmp_timestamp_tags(now, expires)
    assert result == expected


def test_blob_is_valid_passes_with_valid_blob_and_lock():
    """Ensure canonical lock objects with matching and valid values pass."""
    with patch("grpc_lock.server.util.dt") as dt_mock:
        dt_mock.datetime.now.return_value = dt.datetime(1950, 1, 1, 0, 0, 0, 0)
        _test_gen = 123
        _test_id = "alock_id"
        _test_expires = Timestamp(seconds=100, nanos=1)
        _test_expires_tag_value = f"{_test_expires.seconds}.{_test_expires.nanos}"

        blob = Mock()
        blob.generation = _test_gen
        blob.metadata = {
            LOCK_ID_TAG_NAME: _test_id,
            LOCK_EXPIRES_TAG_NAME: _test_expires_tag_value,
        }
        blob.crc32c = LOCK_MAGIC_CRC32C

        lock = GCSLock(
            bucket="unused",
            object="not important",
            generation=_test_gen,
            lock_id=_test_id,
            expires=_test_expires,
        )

        blob_is_valid(blob, lock)


def test_blob_is_valid_raises_on_bad_generation():
    with patch("grpc_lock.server.util.dt") as dt_mock:
        dt_mock.datetime.now.return_value = dt.datetime(1950, 1, 1, 0, 0, 0, 0)
        _test_gen = 123
        _test_id = "alock_id"
        _test_expires = Timestamp(seconds=100, nanos=1)
        _test_expires_tag_value = f"{_test_expires.seconds}.{_test_expires.nanos}"

        blob = Mock()
        blob.generation = 42  # not correct
        blob.metadata = {
            LOCK_ID_TAG_NAME: _test_id,
            LOCK_EXPIRES_TAG_NAME: _test_expires_tag_value,
        }

        blob.crc32c = LOCK_MAGIC_CRC32C

        lock = GCSLock(
            bucket="unused",
            object="not important",
            generation=_test_gen,
            lock_id=_test_id,
            expires=_test_expires,
        )

        with pytest.raises(LockGenerationMismatch):
            blob_is_valid(blob, lock)


def test_blob_is_valid_raises_on_bad_id():
    with patch("grpc_lock.server.util.dt") as dt_mock:
        dt_mock.datetime.now.return_value = dt.datetime(1950, 1, 1, 0, 0, 0, 0)
        _test_gen = 123
        _test_id = "alock_id"
        _test_expires = Timestamp(seconds=100, nanos=1)
        _test_expires_tag_value = f"{_test_expires.seconds}.{_test_expires.nanos}"

        blob = Mock()
        blob.generation = _test_gen
        blob.metadata = {
            LOCK_ID_TAG_NAME: "where'd this come from??",  # not correct
            LOCK_EXPIRES_TAG_NAME: _test_expires_tag_value,
        }

        blob.crc32c = LOCK_MAGIC_CRC32C

        lock = GCSLock(
            bucket="unused",
            object="not important",
            generation=_test_gen,
            lock_id=_test_id,
            expires=_test_expires,
        )

        with pytest.raises(LockIDMismatch):
            blob_is_valid(blob, lock)


def test_blob_is_valid_does_raises_expired():
    with patch("grpc_lock.server.util.dt") as dt_mock:
        dt_mock.datetime.now.return_value = dt.datetime(2000, 1, 1, 0, 0, 0, 0)
        _test_gen = 123
        _test_id = "alock_id"
        _test_expires = Timestamp(seconds=100, nanos=1)
        _test_expires_tag_value = f"{_test_expires.seconds}.{_test_expires.nanos}"

        blob = Mock()
        blob.generation = _test_gen
        blob.metadata = {
            LOCK_ID_TAG_NAME: _test_id,
            LOCK_EXPIRES_TAG_NAME: _test_expires_tag_value,
        }
        blob.crc32c = LOCK_MAGIC_CRC32C

        lock = GCSLock(
            bucket="unused",
            object="not important",
            generation=_test_gen,
            lock_id=_test_id,
            expires=_test_expires,
        )

        with pytest.raises(LockExpired):
            blob_is_valid(blob, lock)


def test_delete_lock_if_expired_deletes_expired_lock():
    with patch("grpc_lock.server.util.blob_is_lock") as blob_is_lock_mock_fn:
        blob_is_lock_mock_fn.return_value = True
        blob = Mock()
        blob.metadata = {LOCK_EXPIRES_TAG_NAME: "-1000.0"}

        delete_lock_if_expired(blob)

        blob.delete.assert_called_once()


def test_delete_lock_if_expired_does_not_delete_running_lock():
    with patch("grpc_lock.server.util.blob_is_lock") as blob_is_lock_mock_fn:
        blob_is_lock_mock_fn.return_value = True
        blob = Mock()
        blob.metadata = {LOCK_EXPIRES_TAG_NAME: f"{1e14}.0"}

        delete_lock_if_expired(blob)

        blob.delete.assert_not_called()


def test_delete_lock_if_expired_does_nothing_on_non_lock():
    with patch("grpc_lock.server.util.blob_is_lock") as blob_is_lock_mock_fn:
        blob_is_lock_mock_fn.side_effect = LockContentsMismatch("not a lock")
        blob = Mock()

        delete_lock_if_expired(blob)

        blob.delete.assert_not_called()
