import logging
from concurrent import futures

import grpc
from google.cloud.storage import Client as GCSClient

from grpc_lock.server.servicer import GrpcLockServicerImplementation
from grpc_lock.server.servicer import add_GrpcLockServiceServicer_to_server

logging.basicConfig(
    format=(
        "%(asctime)s.%(msecs)03d %(levelname)-8s "
        "[%(filename)s:%(funcName)s:%(lineno)d]\n\t%(message)s"
    ),
    datefmt="%Y-%m-%d:%H:%M:%S",
    level=logging.INFO,
)


def serve(gcs_client: GCSClient):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    impl = GrpcLockServicerImplementation(
        gcs_client=gcs_client,
    )

    add_GrpcLockServiceServicer_to_server(impl, server)

    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":

    gcs_client = GCSClient(project="haus-wharf-staging")
    serve(gcs_client=gcs_client)
