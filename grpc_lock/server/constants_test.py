from base64 import b64encode

import google_crc32c as crc32c


def test_constant_defs_exist():
    from .constants import LOCK_EXPIRES_TAG_NAME
    from .constants import LOCK_ID_TAG_NAME
    from .constants import LOCK_MAGIC
    from .constants import LOCK_MAGIC_CRC32C


def test_crc32_matches_magic_contents():

    from .constants import LOCK_MAGIC
    from .constants import LOCK_MAGIC_CRC32C

    cs = crc32c.Checksum(LOCK_MAGIC.encode("utf-8")).digest()

    assert b64encode(cs).decode() == LOCK_MAGIC_CRC32C
