"""Constants used in the generation of GCS lock objects."""

LOCK_MAGIC = "GCS_LOCK"
LOCK_MAGIC_CRC32C = "PxOvpA=="
LOCK_ID_TAG_NAME = "GRPC_LOCK_ID"
LOCK_EXPIRES_TAG_NAME = "GRPC_LOCK_EXPIRES"
