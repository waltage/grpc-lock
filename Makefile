SHELL := /bin/bash

.PHONY: protos

setup:
	pyenv local 3.9
	python3 -m venv .venv
	python3 --version
	ls -la
	. .venv/bin/activate
	cd grpc_lock && poetry install
	@ echo "Now 'source .venv/bin/activate'"

protos: 
	@ ./build_protos.sh
	
test:
	@ cd grpc_lock && python3 -m pytest -c pytest.ini

test-with-cov:
	@ cd grpc_lock && python3 -m pytest -c pytest.ini \
		--cov=grpc_lock \
		--cov-report=term-missing \
		--cov-config=.coveragerc