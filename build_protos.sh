#!/bin/bash

# generate proto files

rm -rf grpc_lock/protos/__pycache__
find grpc_lock/protos/. -type f -name "*_pb2*" -delete


python3 -m grpc_tools.protoc \
    --proto_path=grpc_lock/protos=grpc_lock/protos \
    --python_out=. \
    --pyi_out=. \
    grpc_lock/protos/gcs_lock.proto \
    grpc_lock/protos/grpc_lock_service.proto

# generate gRPC stubs
python3 -m grpc_tools.protoc \
    --proto_path=grpc_lock/protos=grpc_lock/protos \
    --grpc_python_out=. \
    --pyi_out=. \
    grpc_lock/protos/grpc_lock_service.proto